//
//  FirstViewController.h
//  TLP_Tenerife
//
//  Created by gema arnaiz on 9/7/16.
//  Copyright © 2016 gema arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"



@interface FirstViewController : UIViewController<CLLocationManagerDelegate>{
    FIRDatabaseReference *ref;

}
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (strong, nonatomic) CLBeaconRegion *trackingRegion;

//Beacons
@property (strong, nonatomic) CLLocationManager *locationManagerBeacon;

@end

