//
//  SecondViewController.h
//  TLP_Tenerife
//
//  Created by gema arnaiz on 9/7/16.
//  Copyright © 2016 gema arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"



@interface SecondViewController : UIViewController{
    FIRDatabaseReference *ref;

}
@property (weak, nonatomic) IBOutlet UILabel *uidid;
@property (weak, nonatomic) IBOutlet UILabel *minor;
@property (weak, nonatomic) IBOutlet UILabel *major;
@property (weak, nonatomic) IBOutlet UILabel *rssi;
@property (weak, nonatomic) IBOutlet UILabel *proximity;



@end

