//
//  SecondViewController.m
//  TLP_Tenerife
//
//  Created by gema arnaiz on 9/7/16.
//  Copyright © 2016 gema arnaiz. All rights reserved.
//

#import "SecondViewController.h"

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    ref = [[FIRDatabase database] reference];
    
    FIRDatabaseReference *LRef = [ref child:@"beaconInfo"];
    
    [LRef observeEventType:FIRDataEventTypeValue
     withBlock:^(FIRDataSnapshot *snapshot) {
         NSDictionary *beaconDic = snapshot.value;
         _uidid.text =[beaconDic objectForKey:@"uidid"];
         _minor.text =[[beaconDic objectForKey:@"minor"] stringValue];
         _major.text =[[beaconDic objectForKey:@"major"] stringValue];
         _rssi.text =[[beaconDic objectForKey:@"rssi"] stringValue];
         
         _proximity.text =[[beaconDic objectForKey:@"proximity"] stringValue];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
