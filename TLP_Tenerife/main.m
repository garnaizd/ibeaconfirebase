//
//  main.m
//  TLP_Tenerife
//
//  Created by gema arnaiz on 9/7/16.
//  Copyright © 2016 gema arnaiz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
