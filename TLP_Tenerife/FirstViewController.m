//
//  FirstViewController.m
//  TLP_Tenerife
//
//  Created by gema arnaiz on 9/7/16.
//  Copyright © 2016 gema arnaiz. All rights reserved.
//
#import "FirstViewController.h"

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    ref = [[FIRDatabase database] reference];
}

-(void)viewWillAppear:(BOOL)animated{
    self.locationManagerBeacon.delegate = nil;

}

- (IBAction)sendToFirebase:(id)sender {
    [[ref child:@"Devfest texfield"] setValue:@{@"Texto-input": _textField.text}];
    
    [_textField resignFirstResponder];
}

- (IBAction)broadcastiBeacon:(id)sender {
    
    self.locationManagerBeacon = [[CLLocationManager alloc] init];
    self.locationManagerBeacon.delegate = self;
    
    if ([self.locationManagerBeacon respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManagerBeacon requestWhenInUseAuthorization];
    }
    [self.locationManagerBeacon startUpdatingLocation];
    
    //Initialize regions
    [self initRegion];
    
    //Start Monitoring
    [self locationManager:self.locationManagerBeacon didStartMonitoringForRegion:self.trackingRegion];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)initRegion {
    NSLog(@"****** INIT RREGION ********");
    NSUUID *uuidTracking = [[NSUUID alloc] initWithUUIDString:UUID_TRACKING];
    self.trackingRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuidTracking identifier:@"tracking"];
    //NSLog(@"TRACKING UUID : %@", uuidTracking);
    [self.locationManagerBeacon startMonitoringForRegion:self.trackingRegion];
}


#pragma mark - Location
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"location: %@", [locations lastObject]);
}


#pragma mark - CLLocationManager iBeacon
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"REGION %@" , region);
    [self.locationManagerBeacon startRangingBeaconsInRegion:self.trackingRegion];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    NSLog(@"Beacon Found REGION: %@" , region.identifier);
    [self.locationManagerBeacon startRangingBeaconsInRegion:self.trackingRegion];
}


-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    NSLog(@"Left Region: %@", region);
    if ([region.identifier isEqualToString:@"tracking"]) {
        [self.locationManagerBeacon stopRangingBeaconsInRegion:self.trackingRegion];
    }
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    CLBeacon *beacon = [beacons lastObject];
    
    
    if ([beacons count] > 0) {
        //Creamos la estructura de datos que queremos almacenar
        NSDictionary *beaconDic = @{
                                    @"uidid" : beacon.proximityUUID.UUIDString,
                                    @"major" : [NSNumber numberWithLong:[beacon.major longValue]],
                                    @"minor" : [NSNumber numberWithLong:[beacon.minor longValue]],
                                    @"rssi" : [NSNumber numberWithLong:beacon.rssi],
                                    @"proximity" : [NSNumber numberWithLong:beacon.proximity],
                                    };
        //Se guarda la información del diccionario
        [[ref child:@"beaconInfo"] setValue:beaconDic];

    }
}
@end
